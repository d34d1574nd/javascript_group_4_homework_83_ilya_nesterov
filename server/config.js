module.exports = {
  dbUrl: 'mongodb://localhost/melomanFM',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  }
};
